UnicaenInstandia
====

Bibliothèque de messagerie interne à un application.
Initialement développer par Laurent et Bertrand.

Utilisation d'InstaDia
----------------------

La bibliothèque fournie une aide de vue qui met en place le "widget".

```php
echo $this->instadia()->setRubrique('Ma rubrique')->setSousRubrique('Ma sous-rubrique')
    ->setTitle('Zone de dialogue')->setInformation("Message informatif")
    ->setRefreshDelay(1000000)->setHeight(600)->setReadOnly(false); 
```

Les messages sont ensuite enregistrés dans la table unicaen_instadia

Base de données
---------------

Le script de création de la table unicaen_instadia est disponible dans le fichier doc/001_table_SGBD.sql


| Champs        | Type      | Info                                | Commantaire                                      |
|---------------|-----------|-------------------------------------|--------------------------------------------------|
| id            | serial    | primary key                         |                                                  |
| user_id       | int       | foreign key vers ```AbstractUser``` | reférence l'utilisateur ayant rédiger le message |
| rubrique      | string    |                                     | référence le chat                                |
| sous_rubrique | string    | nullable                            | référence le chat                                |
| horodatage    | timestamp | default ```now()```                 | date d'émission du message                       |
| contenu       | text      |                                     | message émis                                     |
---

Javascript et CSS
-----------------

La bibliothèque repose sur un fichier js [public/instadia/js/instadia.js] et un css [public/instadia/css/instadia.css] qu'il faudra copie dans le public du projet.

```bash
cp -r vendor/unicaen/instadia/public public
```

Dépendance
----------

La seule dépendance interne aux bibliothèques unicaen est vers ```UnicaenUtilisateur``` pour la récupération de l'utilisateur mais qui peut être downgradée vers ```UnicaenAuth``` au besoin.
- dans l'entité Instadia [src/UnicaenInstadia/Entity/Db/Instadia.php] et son mapping [src/UnicaenInstadia/Entity/Db/Mapping/UnicaenInstadia.Entity.Db.Instadia.dcm.xml]
  - référence à ```AbstractUser``` : déclaration du lien vers l'utilisateur des messages
- dans le service InstadiaService [src/UnicaenInstadia/Service/Instadia/InstadiaService.php] et sa factory [src/UnicaenInstadia/Service/Instadia/InstadiaServiceFactory.php]
  - référence à ```AbstractUser``` : ajout de l'utilisateur ayant rédiger le message
  - référence à ```UserService``` : récupération de l'utilisateur connecté