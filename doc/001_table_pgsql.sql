create table public.unicaen_instadia
(
    id            serial
        constraint unicaen_instadia_pk primary key,
    user_id       integer                 not null
        constraint unicaen_instadia_redacteur_fk references public.unicaen_utilisateur_user,
    rubrique      varchar(80)             not null,
    sous_rubrique varchar(80),
    horodatage    timestamp default now() not null,
    contenu       text                    not null
);