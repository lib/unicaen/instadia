<?php

namespace UnicaenInstadia\Controller;

use DateTime;
use UnicaenInstadia\Entity\Db\Instadia;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;
use UnicaenInstadia\Service\Instadia\InstadiaServiceAwareTrait;

class InstadiaController extends AbstractActionController
{
    use InstadiaServiceAwareTrait;

    public function indexAction() : JsonModel
    {
        if ($this->params()->fromPost('poster', false)) {
            $this->poster();
        }

        $rubrique     = $this->params()->fromPost('rubrique');
        $sousRubrique = $this->params()->fromPost('sousRubrique');

        $data = $this->getInstadiaService()->getMessages($rubrique, $sousRubrique);

        foreach($data as $i => $d ){
            $data[$i] = $this->getInstadiaService()->instadiaToJson($d);
        }

        return new JsonModel($data);
    }

    protected function poster() : InstadiaController
    {
        $data = $this->params()->fromPost();

        $instadia = new Instadia();
        $this->getInstadiaService()->jsonToInstadia($data, $instadia);
        $instadia->setUser($this->getInstadiaService()->getUser());
        $instadia->setHorodatage(new DateTime());
        $this->getInstadiaService()->save($instadia);

        return $this;
    }
}