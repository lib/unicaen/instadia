<?php
namespace UnicaenInstadia\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenInstadia\Service\Instadia\InstadiaService;

class InstadiaControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return InstadiaController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : InstadiaController
    {
        /**
         * @var InstadiaService $instadiaService
         */
        $instadiaService = $container->get(InstadiaService::class);

        $controller = new InstadiaController();
        $controller->setInstadiaService($instadiaService);

        return $controller;
    }
}
