<?php

namespace UnicaenInstadia\Service\Instadia;

use DateTime;
use DoctrineModule\Persistence\ProvidesObjectManager;
use UnicaenInstadia\Entity\Db\Instadia;
use UnicaenUtilisateur\Entity\Db\AbstractUser;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class InstadiaService
{
    use ProvidesObjectManager;
    use UserServiceAwareTrait;

    private array $callbacks = [];


    /** @return Instadia[] */
    public function getMessages(?string $rubrique = null, ?string $sousRubrique = null) : array
    {
        $params = [
            'rubrique' => $rubrique,
        ];

        if ($sousRubrique) {
            $sousRubriqueDql        = 'AND i.sousRubrique = :sousRubrique';
            $params['sousRubrique'] = $sousRubrique;
        } else {
            $sousRubriqueDql = '';
        }

        $dql = "
        SELECT
          i
        FROM
          UnicaenInstadia\Entity\Db\Instadia i
        WHERE
          i.rubrique = :rubrique
          $sousRubriqueDql
        ORDER BY
          i.horodatage
        ";

        $result = $this->getObjectManager()->createQuery($dql)->setParameters($params)->getResult();

        return $result;
    }



    public function save(Instadia $instadia) : Instadia
    {
        if (!$instadia->getHorodatage()) {
            $instadia->setHorodatage(new DateTime());
        }

        $this->doSend($instadia);

        $this->getObjectManager()->persist($instadia);
        $this->getObjectManager()->flush($instadia);

        return $instadia;
    }



    public function onSend(string $rubrique, string $sousRubrique, callable $callback) : void
    {
        $this->callbacks[$this->makeKey($rubrique, $sousRubrique)] = $callback;
    }

    protected function doSend( Instadia $instadia ) : void
    {
        $key = $this->makeKey($instadia->getRubrique(), $instadia->getSousRubrique());

        if (!array_key_exists($key, $this->callbacks)){
            // si la clé par sous-rubrique n'existe pas alors on prend par rubrique uniquement!!
            $key = $this->makeKey($instadia->getRubrique(), null);
        }

        if (array_key_exists($key, $this->callbacks)){
            $callback = $this->callbacks[$key];
            $callback($instadia);
        }
    }

    protected function makeKey(string $rubrique, string $sousRubrique ) : string
    {
        return $rubrique.'__/__'.$sousRubrique;
    }

    public function getUser() : ?AbstractUser
    {
        if ($this->getUserService()) {
            $user = $this->getUserService()->getConnectedUser();
        } else {
            $user = null;
        }
        return $user;
    }

    public function makeHash(AbstractUser $user) : ?string
    {
        if ($mail = $user->getEmail()) {
            return md5(trim(strtolower($mail)));
        } else {
            return null;
        }
    }

    public function jsonToInstadia( array $json, Instadia $instadia ) : void
    {
        if (isset($json['rubrique'])) $instadia->setRubrique($json['rubrique']);
        if (isset($json['sousRubrique'])) $instadia->setSousRubrique($json['sousRubrique']);
        if (isset($json['contenu'])) $instadia->setContenu($json['contenu']);
    }

    public function instadiaToJson(Instadia $instadia) : array
    {
        $json = [
            'contenu'    => $instadia->getContenu(),
            'horodatage' => $instadia->getHorodatage()->getTimestamp(),
        ];
        if ($user = $instadia->getUser()) {
            $json['user'] = [
                'id'    => $user->getId(),
                'label' => $user->getDisplayName(),
                'hash'  => $this->makeHash($user),
            ];
        }

        return $json;
    }
}
