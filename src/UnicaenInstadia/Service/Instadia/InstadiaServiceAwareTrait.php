<?php

namespace UnicaenInstadia\Service\Instadia;

trait InstadiaServiceAwareTrait
{
    private InstadiaService $serviceInstadia;

    public function getInstadiaService(): InstadiaService
    {
        return $this->serviceInstadia;
    }

    public function setInstadiaService(InstadiaService $serviceInstadia): void
    {
        $this->serviceInstadia = $serviceInstadia;
    }

}
