<?php

namespace UnicaenInstadia\Service\Instadia;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Service\User\UserService;

class InstadiaServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return InstadiaService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : InstadiaService
    {
        /**
         * @var EntityManager $entityManager $service
         * @var UserService $userService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userService = $container->get(UserService::class);

        $service = new InstadiaService();
        $service->setObjectManager($entityManager);
        $service->setUserService($userService);
        return $service;
    }
}

