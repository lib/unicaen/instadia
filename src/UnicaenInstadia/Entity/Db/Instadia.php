<?php

namespace UnicaenInstadia\Entity\Db;

use DateTime;
use UnicaenUtilisateur\Entity\Db\AbstractUser;

class Instadia
{
    private ?int $id = null;
    private ?AbstractUser $user = null;
    private ?string $rubrique = null;
    private ?string $sousRubrique = null;
    private ?DateTime $horodatage = null;
    private ?string $contenu = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getUser(): ?AbstractUser
    {
        return $this->user;
    }

    public function setUser(?AbstractUser $user): void
    {
        $this->user = $user;
    }

    public function getRubrique(): ?string
    {
        return $this->rubrique;
    }

    public function setRubrique(?string $rubrique): void
    {
        $this->rubrique = $rubrique;
    }

    public function getSousRubrique(): ?string
    {
        return $this->sousRubrique;
    }

    public function setSousRubrique(?string $sousRubrique): void
    {
        $this->sousRubrique = $sousRubrique;
    }

    public function getHorodatage(): ?DateTime
    {
        return $this->horodatage;
    }

    public function setHorodatage(?DateTime $horodatage = null): void
    {
        $this->horodatage = $horodatage;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(?string $contenu): void
    {
        $this->contenu = $contenu;
    }


}
