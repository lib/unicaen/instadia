<?php

namespace UnicaenInstadia\View\Helper;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenInstadia\Service\Instadia\InstadiaService;

class InstadiaViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return InstadiaViewHelper
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : InstadiaViewHelper
    {
        $helper = new InstadiaViewHelper();

        /** @var InstadiaService $instadiaService */
        $instadiaService = $container->get(InstadiaService::class);
        $helper->setInstadiaService($instadiaService);

        return $helper;
    }
}
