<?php
namespace UnicaenInstadia\View\Helper;

use Laminas\View\Helper\AbstractHtmlElement;
use UnicaenInstadia\Service\Instadia\InstadiaServiceAwareTrait;

class InstadiaViewHelper extends AbstractHtmlElement
{
    use InstadiaServiceAwareTrait;

    private ?string $rubrique = null;
    private ?string $sousRubrique = null;
    private ?int $refreshDelay = null;
    private ?string $title = null;
    private ?string $information = null;
    private ?int $width = null;
    private ?int $height = null;
    private bool $readOnly = false;

    public function __toString() : string
    {
        return $this->render();
    }

    public function render($rubrique = '__DEFAULT__', $sousRubrique = '__DEFAULT__') : string
    {
        if ($rubrique != '__DEFAULT__') $this->setRubrique($rubrique);
        if ($sousRubrique != '__DEFAULT__') $this->setSousRubrique($sousRubrique);

        $user = $this->getInstadiaService()->getUser();

        $attrs = [
            'class'    => 'instadia',
            'data-url' => $this->getView()->url('instadia'),
        ];

        if ($user) {
            $attrs['data-user-id']    = $user->getId();
            $attrs['data-user-label'] = $user->getDisplayName();
            $attrs['data-user-hash']  = $this->getInstadiaService()->makeHash($user);
        }

        if ($d = $this->getRubrique()) $attrs['data-rubrique'] = $d;
        if ($d = $this->getSousRubrique()) $attrs['data-sous-rubrique'] = $d;
        if ($d = $this->getRefreshDelay()) $attrs['data-refresh-delay'] = $d;
        if ($d = $this->getTitle()) $attrs['data-title'] = $d;
        if ($d = $this->getInformation()) $attrs['data-information'] = $d;
        if ($d = $this->getWidth()) $attrs['data-width'] = $d;
        if ($d = $this->getHeight()) $attrs['data-height'] = $d;
        if (null !== $this->isReadOnly()) $attrs['data-read-only'] = $this->isReadOnly() ? 'true' : 'false';

        $r = (string)$this->getView()->tag('div', $attrs)->html('');

        return $r;
    }

    public function getRubrique() : string
    {
        return $this->rubrique;
    }

    public function setRubrique(string $rubrique) : InstadiaViewHelper
    {
        $this->rubrique = $rubrique;
        return $this;
    }

    public function getSousRubrique() : string
    {
        return $this->sousRubrique;
    }

    public function setSousRubrique(string $sousRubrique) : InstadiaViewHelper
    {
        $this->sousRubrique = $sousRubrique;
        return $this;
    }

    public function getRefreshDelay(): ?int
    {
        return $this->refreshDelay;
    }

    public function setRefreshDelay(?int $refreshDelay): InstadiaViewHelper
    {
        $this->refreshDelay = $refreshDelay;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): InstadiaViewHelper
    {
        $this->title = $title;
        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): InstadiaViewHelper
    {
        $this->information = $information;
        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(?int $width): InstadiaViewHelper
    {
        $this->width = $width;
        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(?int $height): InstadiaViewHelper
    {
        $this->height = $height;
        return $this;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function setReadOnly(bool $readOnly): InstadiaViewHelper
    {
        $this->readOnly = $readOnly;
        return $this;
    }



}
