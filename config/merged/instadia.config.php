<?php

namespace UnicaenInstadia;

use UnicaenInstadia\Controller\InstadiaController;
use UnicaenInstadia\Controller\InstadiaControllerFactory;
use UnicaenInstadia\Service\Instadia\InstadiaService;
use UnicaenInstadia\Service\Instadia\InstadiaServiceFactory;
use UnicaenInstadia\View\Helper\InstadiaViewHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => InstadiaController::class,
                    'action' => [
                        'index',
                    ],
                    'roles' => [],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'instadia' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/instadia',
                    'defaults' => [
                        /** @see InstadiaController::indexAction() */
                        'controller' => InstadiaController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            InstadiaService::class => InstadiaServiceFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            'instadia' => InstadiaViewHelperFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            InstadiaController::class => InstadiaControllerFactory::class,
        ],
    ],

];

